package example.spring.mini_cms;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.LinkedList;
import java.util.List;

@Entity
public class Category {
	@Id
	@GenericGenerator(name="inc" , strategy="increment")
	@GeneratedValue(generator="inc")
	private int id;
	@Column(unique = true)
	private String name;

	@OneToMany(mappedBy = "category")
	private List<Article> articles = new LinkedList<>();

	public Category() { }

	public Category(String name) {
		this.name = name;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}
}

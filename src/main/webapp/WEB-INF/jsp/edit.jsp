<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${not empty article}">
    <c:set var="articleId" value="${article.id}" />
    <c:set var="title" value="${article.title}" />
    <c:set var="content" value="${article.content}" />
    <c:set var="catName" value="${article.category.name}" />
</c:if>
<html>
<head>
    <%@ include file = "refs.jsp" %>
    <title>
        <c:if test="${empty article}">
            New article
        </c:if>
        <c:if test="${not empty article}">
            Edit article: ${title}
        </c:if>
    </title>
</head>
<body>
<%@ include file = "header.jsp" %>
<div class="body">
    <form action="/app/save/${articleId}" method="post">
        <div class="article">
            <div>
                Title:
                <br>
                <input class="input-field" name="title" type="text" value="${title}">
            </div>
            <div >
                Category:
                <br>
                <input class="input-field" name="category" type="text" value="${catName}">
            </div>
            <div>
                Content:
                <br>
                <textarea class="input-field" name="content">${content}</textarea>
            </div>
            <div class="errors">
                <c:forEach items="${errors}" var="error">
                    <div class="error">
                        ${error}
                    </div>
                </c:forEach>
            </div>
            <input class="save-button" type="submit" value="Save article">
        </div>
    </form>
</div>
</body>
</html>

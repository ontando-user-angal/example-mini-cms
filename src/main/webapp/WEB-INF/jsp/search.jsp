<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="refs.jsp" %>
    <title>Search results</title>
</head>
<body>
<%@ include file="header.jsp" %>

<div class="body">
    <div>
        <form action="/app/search" method="get">
            <table>
                <tr>
                    <td>Title:</td>
                    <td><input class="input-field" name="title" type="text" value="${title}"></td>
                </tr>
                <tr>
                    <td>Category:</td>
                    <td><select name="category">
                        <option value="">*Any category*</option>
                        <c:forEach items="${catList}" var="catOpt">
                            <option value="${catOpt}"
                                    <c:if test="${catOpt eq catName}">selected</c:if>
                            >
                                ${catOpt}
                            </option>
                        </c:forEach>
                    </select></td>
                </tr>
                <tr>
                    <td>Content:</td>
                    <td><input class="input-field" name="content" type="text" value="${content}"></td>
                </tr>
                <tr><td></td><td><input type="submit" value="Search"></td></tr>
            </table>
        </form>
    </div>
    <c:forEach items="${articles}" var="article">
        <div>
            <%@ include file="content.jsp" %>
        </div>
    </c:forEach>


    <div class="prev-page-button">
        <a href="?title=${title}&category=${catName}&content=${content}&offset=${prevOffset}">
            <button ${empty prevOffset ? "disabled" : ""}>Prev</button>
        </a>
    </div>
    <div class="next-page-button">
        <a href="?title=${title}&category=${catName}&content=${content}&offset=${nextOffset}">
            <button ${empty nextOffset ? "disabled" : ""}>Next</button>
        </a>
    </div>
</div>
</body>
</html>

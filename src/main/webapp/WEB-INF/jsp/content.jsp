
<div class="article">
    <div class="edit-button"><a href="/app/edit/${article.id}"><button>edit</button></a></div>
    <div class="title">
        <a href="/app/view/${article.id}">${article.title}</a>
    </div>
    <div class="meta">
        <span>Created at : ${article.created}</span>
        <span>in category: <a href="/app/cat/${article.category.name}">${article.category.name}</a></span>
    </div>
    <div class="content">${article.content}</div>
</div>

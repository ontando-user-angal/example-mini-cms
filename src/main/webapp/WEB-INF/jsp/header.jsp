<div class="header">
    <a class="main-page-button" href="/app/all"><button>Main page</button></a>
    <a class="new-article-button" href="/app/new${not empty catName ? "?category=" : ""}${catName}"><button>New article</button></a>
    <a href="/app/search"><button>Search</button></a>
</div>

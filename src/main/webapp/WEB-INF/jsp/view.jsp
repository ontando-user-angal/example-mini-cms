<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file = "refs.jsp" %>
    <title>Article: ${article.title}</title>
</head>
<body>
<%@ include file = "header.jsp" %>

<div class="body">
    <%@ include file = "content.jsp" %>
</div>
</body>
</html>

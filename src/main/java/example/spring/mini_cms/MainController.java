package example.spring.mini_cms;

import org.apache.commons.lang.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Controller
public class MainController {

	public static final Pattern WHITE_SPACE_REGEX = Pattern.compile("\\s");
	public static final int ARTICLES_PER_PAGE = 3;

	@Autowired
	private ArticleManager manager;

	public void setManager(ArticleManager manager) {
		this.manager = manager;
	}

	/**
	 * Controller for page '/app/all'. (Aliases: '/' and '/app').
	 * <br>
	 *
	 * Renders all articles.
	 * <br>
	 *
	 * When there are a lot of articles, they splitted in pages.
	 * Page offset provided vie get variable offset.
	 */
	@GetMapping({"", "/all"})
	public String rootPage(
			Model model,
			@RequestParam(required = false, defaultValue = "0") Integer offset
	) {
		if (offset < 0) {
			offset = 0;
		}
		List<Article> articles = this.manager
				.getArticles(null, offset, MainController.ARTICLES_PER_PAGE);

		model.addAttribute("articles", articles);

		this.calculateOffserAttributes(model, offset, articles);

		return "main";
	}

	/**
	 * Renders all articles in specified category.
	 * <br>
	 *
	 * When there are a lot of articles, they splitted in pages.
	 * Page offset provided vie get variable offset.
	 */
	@GetMapping("/cat/{category}")
	public String catPage(
			Model model,
			@PathVariable String category,
			@RequestParam(required = false, defaultValue = "0") Integer offset
	) {
		if (offset < 0) {
			offset = 0;
		}
		List<Article> articles = this.manager
				.getArticles(category, offset, MainController.ARTICLES_PER_PAGE);

		model.addAttribute("catName", category);
		model.addAttribute("articles", articles);

		this.calculateOffserAttributes(model, offset, articles);

		return "main";
	}

	/**
	 * Provides search page and it's results.
	 * <br>
	 *
	 * When there are a lot of articles, they splitted in pages.
	 * Page offset provided vie get variable offset.
	 *
	 */
	@GetMapping("/search")
	public String searchPage(
			Model model,
			@RequestParam(required = false, defaultValue = "") String title,
			@RequestParam(required = false, defaultValue = "") String category,
			@RequestParam(required = false, defaultValue = "") String content,
			@RequestParam(required = false, defaultValue = "0") Integer offset
	) {
		if (offset < 0) {
			offset = 0;
		}

		category = category.trim();
		if (category.isEmpty()) {
			category = null;
		}
		title = title.trim();
		if (title.isEmpty()) {
			title = null;
		}
		content = content.trim();
		if (content.isEmpty()) {
			content = null;
		}

		List<String> catList = this.manager.getCategories()
				.stream()
				.map(Category::getName)
				.collect(Collectors.toList());

		List<Article> articles = this.manager
				.findArticles(category, title, content, offset, MainController.ARTICLES_PER_PAGE);

		model.addAttribute("catList", catList);
		model.addAttribute("articles", articles);
		model.addAttribute("title", title);
		model.addAttribute("catName", category);
		model.addAttribute("content", content);

		this.calculateOffserAttributes(model, offset, articles);

		return "search";
	}

	/**
	 * Shows view with single article.
	 */
	@GetMapping("/view/{articleId}")
	public String viewPage(Model model, @PathVariable Integer articleId) {
		Article article = this.manager.getArticle(articleId);
		model.addAttribute("article", article);
		return "view";
	}

	/**
	 * Shows article edit view, in article creation form.
	 */
	@GetMapping("/new")
	public String newPage(Model model, @RequestParam(required = false) String category) {
		if (category != null) {
			model.addAttribute("catName", category);
		}
		return "edit";
	}

	/**
	 * Shows article edit view for specified article.
	 */
	@GetMapping("/edit/{articleId}")
	public String editPage(Model model, @PathVariable Integer articleId) {
		Article article = this.manager.getArticle(articleId);
		model.addAttribute("article", article);
		return "edit";
	}

	/**
	 * Saves chages od article modification (creation).
	 *
	 * If there are incorrect user inputs, article edit
	 * form re-displayed with appropriate error messages.
	 */
	@PostMapping({"/save", "/save/{articleId}"})
	public String savePage(
			Model model,
			@PathVariable(required = false) Integer articleId,
			@RequestParam String title,
			@RequestParam String content,
			@RequestParam String category
	) {
		List<String> errors = new LinkedList<>();
		if (title == null || title.trim().isEmpty()) {
			title = "{No title}";
		}
		title = StringEscapeUtils.escapeHtml(title);
		if (title.length() > 255) {
			errors.add("Title to long (" + title.length() + " > 255)!");
		}

		if (content == null || content.trim().isEmpty()) {
			content = "{No content}";
		}
		content = StringEscapeUtils.escapeHtml(content);
		if (content.length() > 10000) {
			errors.add("Content to long (" + content.length() + " > 10000)!");
		}

		if (category == null || category.trim().isEmpty()) {
			category = "";
			errors.add("Category not set!");
		}
		category = category.trim();
		if (MainController.WHITE_SPACE_REGEX.matcher(category).find()) {
			errors.add("Category contains spaces!");
		}
		category = StringEscapeUtils.escapeHtml(category);

		if (!errors.isEmpty()) {
			if (articleId != null) {
				model.addAttribute("articleId", articleId);
			}
			model.addAttribute("title", title);
			model.addAttribute("content", content);
			model.addAttribute("catName", category);
			model.addAttribute("errors", errors);
			return "edit";
		}

		Article article;
		if (articleId == null) {
			article = this.manager.createArticle(title, content, category);
		} else {
			try {
				article = this.manager.updateArticle(articleId, title, content, category);
			} catch (IllegalArgumentException e) {
				errors.add("Internal error: incorrect article id");
				if (articleId != null) {
					model.addAttribute("articleId", articleId);
				}
				model.addAttribute("title", title);
				model.addAttribute("content", content);
				model.addAttribute("catName", category);
				model.addAttribute("errors", errors);
				return "edit";
			}
		}
		model.addAttribute("article", article);
		return "view";
	}


	/**
	 * Calculates offsets (for next and prev pages) for new list and saves it's into model.
	 */
	private void calculateOffserAttributes(
			Model model,
			Integer offset,
			List<Article> articles
	) {
		if (offset > 0) {
			// We are not at first page.
			if (offset > MainController.ARTICLES_PER_PAGE) {
				// Calculating offset of previous page.
				model.addAttribute(
						"prevOffset",
						offset - MainController.ARTICLES_PER_PAGE
				);
			} else {
				// We are betwin first and second page. Previous page will be al 0.
				model.addAttribute("prevOffset", 0);
			}
		}
		if (articles.size() >= MainController.ARTICLES_PER_PAGE) {
			// Page is full. It might be articles on next page.
			model.addAttribute(
					"nextOffset",
					offset + MainController.ARTICLES_PER_PAGE
			);
		}
	}

}

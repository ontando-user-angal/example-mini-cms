<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="refs.jsp" %>
    <title>
        <c:if test="${empty catName}">
            All posts
        </c:if>
        <c:if test="${not empty catName}">
            Posts: ${catName}
        </c:if>
    </title>
</head>
<body>
<%@ include file="header.jsp" %>

<div class="body">
    <c:if test="${not empty catName}">
        <h2 class="category-title">Category: ${catName}</h2>
    </c:if>

    <c:forEach items="${articles}" var="article">
        <div>
            <%@ include file="content.jsp" %>
        </div>
    </c:forEach>


    <div class="prev-page-button">
        <a href="?offset=${prevOffset}">
            <button ${empty prevOffset ? "disabled" : ""}>Prev</button>
        </a>
    </div>
    <div class="next-page-button">
        <a href="?offset=${nextOffset}">
            <button ${empty nextOffset ? "disabled" : ""}>Next</button>
        </a>
    </div>
</div>
</body>
</html>

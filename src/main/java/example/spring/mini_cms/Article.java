package example.spring.mini_cms;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
public class Article {

	@Id
	@GenericGenerator(name="inc" , strategy="increment")
	@GeneratedValue(generator="inc")
	private int id;
	private String title;
	@Column(columnDefinition = "TEXT", length = 10000)
	private String content;
	private LocalDateTime created;
	@ManyToOne(fetch = FetchType.EAGER)
	private Category category;

	public Article() {}

	public Article(String title, String content, LocalDateTime created, Category category) {
		this.title = title;
		this.content = content;
		this.created = created;
		this.category = category;
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public LocalDateTime getCreated() {
		return this.created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public Category getCategory() {
		return this.category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}
}

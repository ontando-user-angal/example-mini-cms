package example.spring.mini_cms;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.query.Query;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;

import javax.persistence.NoResultException;
import java.io.Closeable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Manager for articles and categories.
 *<br>
 * Provide API for article and category storage and management,
 *<br>
 * Stroes data in H2 database via Hebernate.
 */
public class ArticleManager implements AutoCloseable, Closeable {
	private SessionFactory factory;
	private final String filePath;

	/**
	 *
	 * @param filePath Path to h2 database in apropriate formt (ie './data').
	 */
	public ArticleManager(String filePath) {
		this.filePath = filePath;
	}

	/**
	 * Instantiate session factory.
	 */
	public void initSessionFactory() {
		StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
				.applySetting("hibernate.connection.driver_class", "org.h2.Driver")
				.applySetting("hibernate.connection.url", "jdbc:h2:" + filePath)
				.applySetting("hibernate.connection.pool_size", "5")
				.applySetting("hibernate.dialect", "org.hibernate.dialect.H2Dialect")
				.applySetting("hibernate.cache.provider_class", "org.hibernate.cache.internal.NoCacheProvider")
				.applySetting("hibernate.show_sql", "true")
				.applySetting("hibernate.hbm2ddl.auto", "update")
				.build();

		SessionFactory factory;

		try {
			factory = new MetadataSources(registry)
					.addAnnotatedClass(Category.class)
					.addAnnotatedClass(Article.class)
					.buildMetadata()
					.buildSessionFactory();
		} catch (Exception e) {
			StandardServiceRegistryBuilder.destroy( registry );
			throw e;
		}
		this.factory = factory;
	}

	/**
	 * For tests.
	 */
	protected Session openSession() {
		return this.factory.openSession();
	}


	/**
	 * Returns all categories, that ever exists in db.
	 */
	public List<Category> getCategories() {
		try (Session session = this.factory.openSession()) {
			Transaction transaction = session.beginTransaction();
			try {
				List<Category> categories;
				categories = session.createQuery(
						"select c "
						+ " from Category c "
						+ " order by c.name asc",
						Category.class
				)
						.getResultList();

				transaction.commit();
				return categories;
			} catch (Exception e) {
				transaction.rollback();
				throw e;
			}
		}
	}

	/**
	 * Get articles, sorted by date descending.
	 * <br>
	 * If category provided, returns articles from this category.
	 * <br>
	 * Limit and offset specifies amount of entities to return.
	 */
	public List<Article> getArticles(@Nullable String category, int offset, int limit) {
		try (Session session = this.factory.openSession()) {
			Transaction transaction = session.beginTransaction();
			try {

				List<Article> articles;
				if (category == null) {
					articles = session.createQuery(
							"select a "
							+ " from Article a "
							+ " join a.category c "
							+ " order by a.created desc",
							Article.class
					)
							.setFirstResult(offset)
							.setMaxResults(limit)
							.getResultList();
				} else {
					articles = session.createQuery(
							"select a "
							+ " from Article a "
							+ " join a.category c "
							+ " where c.name = :name "
							+ " order by a.created desc",
							Article.class
					)
							.setParameter("name", category)
							.setFirstResult(offset)
							.setMaxResults(limit)
							.getResultList();
				}
				transaction.commit();
				return articles;
			} catch (Exception e) {
				transaction.rollback();
				throw e;
			}
		}
	}

	/**
	 * Searches for articles, sorted by date descending.
	 * <br>
	 * Category, title and offset (if provided) used for queering matching articles.
	 * Category must be full matched category name of article.
	 * Title and content are checked for occurrence in appropriate fields.
	 * <br>
	 * Limit and offset specifies amount of entities to return.
	 */
	public List<Article> findArticles(
			@Nullable String category,
			@Nullable String title,
			@Nullable String content,
			int offset,
			int limit
	) {
		try (Session session = this.factory.openSession()) {
			Transaction transaction = session.beginTransaction();
			try {


				List<Article> articles;

				Query<Article> query = session.createQuery(
						"select a "
						+ " from Article a "
						+ " join a.category c "
						+ " where 1 = 1 "
						+ (category == null ? "" : " and c.name = :name ")
						+ (title == null ? "" : " and a.title like :title ")
						+ (content == null ? "" : " and a.content like :content ")
						+ " order by a.created desc",
						Article.class
				);
				if (category != null) {
					query.setParameter("name", category);
				}
				if (title != null) {
					query.setParameter("title", "%" + title + "%");
				}
				if (content != null) {
					query.setParameter("content", "%" + content + "%");
				}

				articles = query
						.setFirstResult(offset)
						.setMaxResults(limit)
						.getResultList();
				transaction.commit();
				return articles;
			} catch (Exception e) {
				transaction.rollback();
				throw e;
			}
		}
	}

	/**
	 * Returns article with specified id.
	 */
	public Article getArticle(int articleId) {
		try (Session session = this.factory.openSession()) {
			Transaction transaction = session.beginTransaction();
			try {
				Article article = session.get(Article.class, articleId);
				transaction.commit();
				return article;
			} catch (Exception e) {
				transaction.rollback();
				throw e;
			}
		}
	}

	/**
	 * Creates new article.
	 * <br>
	 * If category with this name exists, it's used as article category,
	 * otherwise new category created for this article.
	 */
	public Article createArticle(String title, String content, @NonNull String categoryName) {
		try (Session session = this.factory.openSession()) {

			Transaction transaction = session.beginTransaction();
			try {
				Article article = null;

				Category category = this.getOrCreateCategory(session, categoryName);


				article = new Article(title, content, LocalDateTime.now(), category);

				session.save(article);

				transaction.commit();
				return article;
			} catch (Exception e) {
				transaction.rollback();
				throw e;
			}
		}
	}

	/**
	 * Updated article title, content and category.
	 * <br>
	 * If category with this name exists, it's used as article category,
	 * otherwise new category cteated.
	 * <br>
	 * Previous article category keepet as is, even it
	 * was last article and now it's empty.
	 */
	public Article updateArticle(int articleId, String title, String content, String categoryName) {

		try (Session session = this.factory.openSession()) {

			Transaction transaction = session.beginTransaction();
			try {
				Article article = session.get(Article.class, articleId);
				if (article == null) {
					throw new IllegalArgumentException("Incorrect article id");
				}

				Category category = this.getOrCreateCategory(session, categoryName);

				article.setTitle(title);
				article.setContent(content);
				article.setCategory(category);

				session.update(article);

				transaction.commit();
				return article;
			} catch (Exception e) {
				transaction.rollback();
				throw e;
			}
		}
	}

	/**
	 * Returns category with this name.
	 *
	 * If category not exists, it will be created.
	 */
	private Category getOrCreateCategory(Session session, String categoryName) {
		try {
			Category result = session.createQuery(
					"select c"
					+ " from Category c"
					+ " where c.name = :name",
					Category.class
			)
					.setParameter("name", categoryName)
					.getSingleResult();
			return result;
		} catch (NoResultException e) {
			Category category = new Category(categoryName);
			Integer id = (Integer) session.save(category);
			category.setId(id);
			return category;
		}
	}

	/**
	 * Closeable's close to close factory. For use as @Bean.
	 */
	@Override
	public void close() {
		this.factory.close();
	}
}

package example.spring.mini_cms;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * Spring MVC configuration.
 *
 * Registering bean {@link ArticleManager} for use in {@link MainController}.
 */
@Configuration
// Scanning current package for additional servlet components.
@ComponentScan(basePackages = "example.spring.mini_cms")
public class MiniCMSConfiguration implements WebMvcConfigurer {

	@Bean
	public ArticleManager articleManager() {
		ArticleManager manager = new ArticleManager("./data");
		manager.initSessionFactory();
		return manager;
	}

	/**
	 * ViewResolver bean for handle JST/JSTL view files.
	 */
	@Bean
	public ViewResolver getViewResolver() {
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setViewClass(JstlView.class);
		resolver.setPrefix("/WEB-INF/jsp/");
		resolver.setSuffix(".jsp");
		return resolver;
	}

	/**
	 * Mapping for static resoucres.
	 */
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**", "/resources/");
	}

}

package example.spring.mini_cms;

import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.File;
import java.time.LocalDateTime;

/**
 * Hibernate test.
 */
public class Test {
	public static void main(String[] args) {
		File file = new File("./test_data.mv.db");
		file.delete();
		if (file.exists()) {
			throw new RuntimeException("file exists");
		}
		try (ArticleManager manager = new ArticleManager("./test_data")) {
			manager.initSessionFactory();
			try (Session session = manager.openSession()) {
				Transaction transaction = session.beginTransaction();
				try {

					printAll(session);

					Category test = new Category("test");
					int id = (Integer) session.save(test);
					System.out.println(test.getId());
					System.out.println(id);
					Article article = new Article("123", "111222333", LocalDateTime.now(), test);
					int aid = (Integer) session.save(article);
					System.out.println(article.getId());
					System.out.println(aid);

					printAll(session);

					transaction.commit();
				} catch (Exception e) {
					e.printStackTrace();
					transaction.rollback();
					throw e;
				}
			}

			manager.createArticle("Test article", "123\n456\n789", "News");
			manager.createArticle("Other article", "123\n456\n789", "News");

			try (Session session = manager.openSession()) {
				printAll(session);
			}
		}
	}

	private static void printAll(Session session) {
		for (Category category : session.createQuery(
				"select c from Category c",
				Category.class
		).getResultList()) {
			System.out.println("   [c] " + category.getId() + ": " + category.getName());
		}
		for (Article article : session.createQuery(
				"select c from Article c",
				Article.class
		).getResultList()) {
			System.out.println("   [a] " + article.getId() + ": " + article.getTitle() + " (" + article.getCategory().getId() + ")");
		}
	}
}

package example.spring.mini_cms;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletRegistration;

/**
 * Spring servlet initializer.
 * <br>
 * Registers Spring MVC {@link DispatcherServlet} and apply it basic configuration.
 * <br>
 * Some additional configuration are applied from {@link MiniCMSConfiguration}.
 */
public class MiniCMSAppInitializer implements WebApplicationInitializer {

	@Override
	public void onStartup(ServletContext container) {


		AnnotationConfigWebApplicationContext ac = new AnnotationConfigWebApplicationContext();
		// Registering MiniCMSConfiguration to apply configuration from it.
		ac.register(MiniCMSConfiguration.class);
		ac.refresh();

		DispatcherServlet servlet = new DispatcherServlet(ac);
		ServletRegistration.Dynamic registration
				= container.addServlet("cms", servlet);

		registration.setLoadOnStartup(1);
		// Registering '/' to alias it to '/app/all'
		// All other bindings are set in '/app/*'
		registration.addMapping("", "/app/*");
	}


}
